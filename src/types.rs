use clap::Parser;
use serde::{Deserialize, Serialize};

/// Return if there is any installed orphan AUR package
#[derive(Parser, Clone, Debug)]
#[clap(version)]
pub struct Args {
    /// Enable verbose mode, this will print the current maintainer
    /// of all aur packages or "package not found"
    #[clap(short, long)]
    pub verbose: bool,
    #[clap(short, long)]
    /// Just check the specified package
    pub package: Option<String>,
    /// Aurweb url
    #[clap(short, long, default_value = "https://aur.archlinux.org")]
    pub aurweb_url: String,
    /// Send a desktop notification if an orphan package is found that will
    /// be shown the given seconds
    #[clap(short, long)]
    pub notify: Option<u32>,
    /// Send a persisten destop notification if an orphan package is found
    #[clap(short = 'N', long = "Notify")]
    pub persistent_notify: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Package {
    #[serde(default, rename = "Depends")]
    depends: Vec<String>,
    #[serde(rename = "Description")]
    description: String,
    #[serde(rename = "FirstSubmitted")]
    first_submitted: i32,
    #[serde(rename = "ID")]
    id: i32,
    #[serde(rename = "Keywords")]
    keywords: Vec<String>,
    #[serde(rename = "LastModified")]
    last_modified: i32,
    #[serde(rename = "License")]
    license: Vec<String>,
    #[serde(rename = "Maintainer")]
    pub maintainer: Option<String>,
    #[serde(default, rename = "MakeDepends")]
    make_depends: Vec<String>,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "NumVotes")]
    pub num_votes: i32,
    #[serde(rename = "OutOfDate")]
    pub out_of_date: Option<i32>,
    #[serde(rename = "PackageBase")]
    package_base: String,
    #[serde(rename = "PackageBaseID")]
    package_base_id: i32,
    #[serde(rename = "Popularity")]
    popularity: f64,
    #[serde(rename = "Submitter")]
    submitter: String,
    #[serde(rename = "URL")]
    url: String,
    #[serde(rename = "URLPath")]
    pub url_path: String,
    #[serde(rename = "Version")]
    version: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AurResults {
    #[serde(rename = "resultcount")]
    pub result_count: i32,
    pub results: Vec<Package>,
    r#type: String,
    version: i32,
}
