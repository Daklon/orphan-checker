rm -r signing
mkdir signing
cd signing
glab release download $1
for file in *; do
    gpg --output $file.asc --detach-sig -u 1093631AE70F27075947C8DEC47377D56A22D749 --sign $file
    gpg --verify $file.asc $file
done
glab release upload $1 *.asc
