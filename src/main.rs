mod types;
use clap::Parser;
use notify_rust::{Hint, Notification, Timeout, Urgency};
use std::io::Error;
use std::process::Command;
use types::{Args, AurResults};

fn main() -> Result<(), Error> {
    let args = Args::parse();
    let mut packages = Vec::new();
    let args_copy = args.clone();
    match args_copy.package {
        Some(package) => packages.push(package),
        None => packages = get_installed_aur_packages()?,
    }
    let aur_packages = match get_aur_packages(packages, &args.aurweb_url) {
        Ok(data) => data,
        Err(error) => panic!("Error retrieving data: {}", error),
    };
    match check_aur_package_results(aur_packages, &args) {
        Some(output) => {
            output_results(output, args);
            std::process::exit(1)
        }
        None => std::process::exit(0),
    }
}

fn output_results(data: String, args: Args) {
    if let Some(timeout) = args.notify {
        Notification::new()
            .summary("Orphan packages found")
            .body(&data)
            .timeout(Timeout::Milliseconds(timeout)) //milliseconds
            .show()
            .unwrap();
    } else if args.persistent_notify {
        Notification::new()
            .summary("Orphan packages found")
            .body(&data)
            .hint(Hint::Resident(true)) // this is not supported by all implementations
            .hint(Hint::Urgency(Urgency::Critical))
            .timeout(Timeout::Never) // this however is
            .show()
            .unwrap();
    } else {
        println!("{}", data);
    }
}

fn get_installed_aur_packages() -> Result<Vec<String>, Error> {
    let packages = Command::new("pacman").arg("-Qmq").output()?;
    let packages: Vec<String> = String::from_utf8_lossy(&packages.stdout)
        .split('\n')
        .map(|s| s.to_string())
        .collect();
    Ok(packages)
}

fn get_aur_packages(packages: Vec<String>, url: &String) -> Result<AurResults, minreq::Error> {
    let url = format!("{}{}", url, "/rpc/?v=5&type=info");
    let mut request = minreq::get(url);
    for package in packages.into_iter() {
        request = request.with_param("arg[]", package);
    }
    let response = request.send();
    match response {
        Ok(data) => data.json::<AurResults>(),
        Err(error) => Err(error),
    }
}

fn check_aur_package_results(aur_json: AurResults, args: &Args) -> Option<String> {
    let mut output = String::new();
    for package in aur_json.results {
        match package.maintainer {
            Some(maintainer) => {
                if args.verbose {
                    println!("{}: {}", package.name, maintainer);
                }
            }
            None => {
                let mut color = String::new();
                let mut end_color = String::new();
                if args.notify.is_none() && !args.persistent_notify {
                    color = if package.out_of_date.is_none() {
                        "\x1B[38;2;0;255;0m".to_string()
                    } else {
                        "\x1B[38;2;255;0;0m".to_string()
                    };
                    end_color = String::from("\x1B[0m");
                }
                output.push_str(&format!(
                    "{0}/packages/{1}\t{2}{3} votes{4}\n",
                    args.aurweb_url, package.name, color, package.num_votes, end_color
                ));
            }
        };
    }
    if !output.is_empty() {
        Some(output)
    } else {
        None
    }
}
